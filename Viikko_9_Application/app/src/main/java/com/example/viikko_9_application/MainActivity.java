package com.example.viikko_9_application;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.EditTextPreference;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class MainActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, View.OnClickListener {

    private TextView title;
    private Button startTimePicker;
    private Button endTimePicker;
    private Spinner theaterSpinner;
    private Button startDateButton;
    private Button endDateButton;
    private EditText editText;
    private RecyclerView recyclerView;

    private String spinnerSelect;
    private int currTheaterID;

    private ArrayList<Movie> movieArray = new ArrayList<Movie>();

    private String chosenDate;
    private String startTime;
    private String endTime;

    private Date currDate = Calendar.getInstance().getTime();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        startTimePicker = (Button)findViewById(R.id.startTimeButton);
        endTimePicker = (Button)findViewById(R.id.endTimeButton);
        title = (TextView)findViewById(R.id.title);
        theaterSpinner = (Spinner)findViewById(R.id.spinner);
        startDateButton = (Button)findViewById(R.id.startDateBtn);
        editText = (EditText)findViewById(R.id.editTextTextPersonName);
        recyclerView = (RecyclerView)findViewById(R.id.recycleView);

        String date = currDate.toString().split(" ")[1] +" "+ currDate.toString().split(" ")[2];
        startDateButton.setText(date);

        startTimePicker.setOnClickListener(this);
        startDateButton.setOnClickListener(this);
        endTimePicker.setOnClickListener(this);

        TheaterDetails theaterDetails = new TheaterDetails();
        XMLparser xmLparser = new XMLparser();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, theaterDetails.getTheaterAreasNames());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        theaterSpinner.setAdapter(adapter);

        theaterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerSelect = parent.getItemAtPosition(position).toString();
                if (!spinnerSelect.equals("Valitse alue/teatteri")) {
                    movieArray.clear();
                    for (int theaterID : theaterDetails.getTheaterAreas().keySet()) {
                        if (spinnerSelect.equals(theaterDetails.getTheaterAreas().get(theaterID))) {
                            currTheaterID = theaterID;
                            System.out.println("Date: "+chosenDate);
                            System.out.println("Time: "+startTime+" - "+endTime);
                            movieArray = xmLparser.readShowings(theaterID, chosenDate, startTime, endTime);
                        }
                    }
                    MyRecycleViewAdapter rvAdapter = new MyRecycleViewAdapter(getApplicationContext(), movieArray);
                    recyclerView.setAdapter(rvAdapter);
                } else {
                    if (movieArray.size() != 0) {
                        movieArray.clear();
                    }
                    MyRecycleViewAdapter rvAdapter = new MyRecycleViewAdapter(getApplicationContext(), movieArray);
                    recyclerView.setAdapter(rvAdapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String d = String.valueOf(dayOfMonth), m = String.valueOf(month+1);
        if (String.valueOf(dayOfMonth).length() == 1) {
            d = "0"+dayOfMonth;
        }

        if (String.valueOf(month).length() == 1) {
            m = "0"+m;
        }
        chosenDate = d+"."+m+"."+year;
        startDateButton.setText(chosenDate);
        updateRecycleView();
    }

    private void updateRecycleView() {
        if (!spinnerSelect.equals("Valitse alue/teatteri") && currTheaterID != 0) {
            movieArray.clear();
            XMLparser xmLparser = new XMLparser();
            movieArray = xmLparser.readShowings(currTheaterID, chosenDate, startTime, endTime);
            MyRecycleViewAdapter rvAdapter = new MyRecycleViewAdapter(getApplicationContext(), movieArray);
            recyclerView.setAdapter(rvAdapter);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == startTimePicker) {
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @SuppressLint("DefaultLocale")
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            startTime = String.format("%02d:%02d:00", hourOfDay, minute);
                            startTimePicker.setText(startTime);
                            updateRecycleView();
                        }
                    }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), true);
            timePickerDialog.show();
        }

        if (v == startDateButton) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(
                    this,
                    this,
                    Calendar.getInstance().get(Calendar.YEAR),
                    Calendar.getInstance().get(Calendar.MONTH),
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
            );
            datePickerDialog.show();
        }

        if (v == endTimePicker) {
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @SuppressLint("DefaultLocale")
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            endTime = String.format("%02d:%02d:00", hourOfDay, minute);
                            endTimePicker.setText(endTime);
                            updateRecycleView();
                        }
                    }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), true);
            timePickerDialog.show();
        }
    }


}