package com.example.viikko_9_application;

import android.view.View;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class XMLparser {

    private static final String THEATER_AREA_URL = "https://www.finnkino.fi/xml/TheatreAreas/";
    private static final String SHOWINGS_BY_DATE_URL_START = "https://www.finnkino.fi/xml/Schedule/?area=";

    public ArrayList<Movie> readShowings(int id, String date, String sTime, String eTime) {

        ArrayList<Movie> movieArray = new ArrayList<Movie>();
        String url_end = id + "&dt=" + date;
        String url = SHOWINGS_BY_DATE_URL_START + url_end;
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(url);
            doc.getDocumentElement().normalize();
            //System.out.println("Root element: " + doc.getDocumentElement().getNodeName());

            NodeList nList =  doc.getDocumentElement().getElementsByTagName("Show");

            for (int i = 0 ; i < nList.getLength() ; i++) {
                Node node = nList.item(i);
                //System.out.println("element: " + node);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    String title = element.getElementsByTagName("Title").item(0).getTextContent();
                    String startTime = element.getElementsByTagName("dttmShowStart").item(0).getTextContent().split("T")[1].substring(0, 5);
                    String endTime = element.getElementsByTagName("dttmShowEnd").item(0).getTextContent().split("T")[1].substring(0, 5);
                    String fsTime = element.getElementsByTagName("dttmShowStart").item(0).getTextContent().split("T")[1];

                    if (sTime == null || eTime == null) {
                        movieArray.add(new Movie(title+" ("+startTime+" - "+endTime+")"));
                        //System.out.println(title + " : " + startTime + " : " + endTime);
                    } else if (Time.valueOf(fsTime).after(Time.valueOf(sTime)) && Time.valueOf(fsTime).before(Time.valueOf(eTime))) {
                        movieArray.add(new Movie(title+" ("+startTime+" - "+endTime+")"));
                        //System.out.println(title + " : " + startTime + " : " + endTime);
                    }

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } finally {
            System.out.println("#### DONE ####");
        }
        return movieArray;

    }

    public HashMap<Integer, String> readTheaterArea() {
        HashMap<Integer, String> theaterAreas = new HashMap<Integer, String>();
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(THEATER_AREA_URL);
            doc.getDocumentElement().normalize();
            //System.out.println("Root element: " + doc.getDocumentElement().getNodeName());

            NodeList nList =  doc.getDocumentElement().getElementsByTagName("TheatreArea");

            for (int i = 0 ; i < nList.getLength() ; i++) {
                Node node = nList.item(i);
                //System.out.println("element: " + node);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;

                    int id = Integer.parseInt(element.getElementsByTagName("ID").item(0).getTextContent());
                    //System.out.println("ID: " + id);

                    String name = element.getElementsByTagName("Name").item(0).getTextContent();
                    //System.out.println("Location: " + name);
                    //theaters.getTheaterAreas().put(id, name);
                    //TheaterDetails.getTheaterAreas().put(id, name);
                    theaterAreas.put(id, name);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } finally {
            System.out.println("#### DONE ####");
        }
        return theaterAreas;
    }

}
