package com.example.viikko_9_application;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class TheaterDetails {

    XMLparser xml = new XMLparser();

    private final HashMap<Integer, String> theaterAreas = xml.readTheaterArea();

    public HashMap<Integer, String> getTheaterAreas() {
        return theaterAreas;
    }

    public List<String> getTheaterAreasNames() {
        return new ArrayList<String>(theaterAreas.values());
    }

    public ArrayList<Integer> getTheaterIDs() {
        return new ArrayList<Integer>(theaterAreas.keySet());
    }
}
