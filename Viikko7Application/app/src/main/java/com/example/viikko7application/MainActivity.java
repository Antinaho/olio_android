package com.example.viikko7application;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView text;
    TextView userInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (TextView) findViewById(R.id.textView2);
        userInput = (TextView) findViewById(R.id.editTextTextPersonName);

        userInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                text.setText(userInput.getText());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void testFunction(View v) {
        System.out.println("Hello World!");
        text.setText(userInput.getText());
    }

}