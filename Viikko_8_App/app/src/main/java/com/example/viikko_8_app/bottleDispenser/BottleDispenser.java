package com.example.viikko_8_app.bottleDispenser;

import android.annotation.SuppressLint;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

public class BottleDispenser {

    private static final String moneyStart = "You have: ";

    private static BottleDispenser instance;

    private double money;
    private ArrayList<Bottle> bottle_array;

    public BottleDispenser() {

        money = 0;

        bottle_array = new ArrayList<Bottle>();

        bottle_array.add(new Bottle());
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 0.9, 1.5, 2.2));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.30, 0.5, 2.0));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.80, 1.5, 2.5));
        bottle_array.add(new Bottle("Fanta Zero", "Fanta", 0.28, 0.5, 1.95));
    }

    public static BottleDispenser getInstance() {
        if (instance == null) {
            instance = new BottleDispenser();
        }
        return instance;
    }

    @SuppressLint("SetTextI18n")
    public void addMoney(TextView outputTxtV, TextView moneyTxtV, int moneyAdded) {
        money += moneyAdded;
        //System.out.println("Klink! Added more money!");

        moneyTxtV.setText(moneyStart + getMoney());
        outputTxtV.setText("Added "+ moneyAdded + " €");
    }

    @SuppressLint("SetTextI18n")
    public void buyBottle(TextView outputTxtV, TextView moneyTxtV, String[] bottleAttributes) {
        if (!checkAvailability(bottleAttributes) && bottle_array.size() > 0) {
            outputTxtV.setText("Selected item is out of stock. Choose another item.");
            //System.out.println("Looked for: "+Arrays.toString(bottleAttributes));
            return;
        }

        //System.out.println("bottle available");

        if (bottle_array.size() > 0) {
            Bottle current = getCurrent(bottleAttributes);
            if (current == null) {
                System.out.println("Something went wrong, stopping");
                System.exit(-1);
            }

            if ( money <= 0 || money < current.getPrice()) {
                //System.out.println("Add money first!");
                outputTxtV.setText("Add money first!");
                return;
            }

            if (bottle_array.size() > 0) {
                //System.out.println("KACHUNK! "+current.getName()+" came out of the dispenser!");
                money -= bottle_array.get(bottle_array.indexOf(current)).getPrice();
                bottle_array.remove(current);
                outputTxtV.setText("KACHUNK! "+current.getName()+" came out of the dispenser!");
                moneyTxtV.setText(moneyStart + getMoney());
            }
        }
        else {
            outputTxtV.setText("No bottles left");
            //System.out.println("No bottles left");
        }


    }

    private Bottle getCurrent(String[] bottleAttributes) {
        String bName = bottleAttributes[0];
        String bSize = bottleAttributes[1].replace(" ", "").replace("l", "");
        String bPrice = bottleAttributes[2].replace(" ", "");

        for (Bottle bottle : bottle_array) {
            String name = bottle.getName();
            double size = bottle.getSize();
            double price = bottle.getPrice();
            System.out.println("get current: "+bName+ " "+bSize+ " "+bPrice);

            if (name.equals(bName) && String.valueOf(size).equals(bSize) && String.valueOf(price).equals(bPrice)) {
                return bottle;
            }
        }
        return null;
    }

    @SuppressLint("SetTextI18n")
    public void returnMoney(TextView outputTxtV, TextView moneyTxtV) {
        String rounded = String.format("%.2f", money).replace(".", ",");
        System.out.println("Klink klink. Money came out! You got "+rounded+"€ back");
        outputTxtV.setText("Returned " + money + " €");
        money = 0;
        moneyTxtV.setText(moneyStart + getMoney());

    }

    public boolean checkAvailability(String[] bottleAttributes) {
        String bName = bottleAttributes[0];
        String bSize = bottleAttributes[1].replace(" ", "").replace("l", "");
        String bPrice = bottleAttributes[2].replace(" ", "");

        for (Bottle bottle : bottle_array) {
            String name = bottle.getName();
            String size = String.valueOf(bottle.getSize());
            String price = String.valueOf(bottle.getPrice());
            //System.out.println("check: "+bName+"///"+name +" : "+bSize+"///"+size +" : "+bPrice+"///"+price +" : ");

            if (name.equals(bName) && size.equals(bSize) && price.equals(bPrice)) {
                System.out.println("found: "+name+ " "+size+ " "+price);
                //can buy
                return true;
            }

        }
        return false;
    }

    public double getMoney() {
        return money;
    }




}
