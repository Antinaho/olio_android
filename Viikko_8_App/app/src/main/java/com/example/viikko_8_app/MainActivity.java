package com.example.viikko_8_app;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.viikko_8_app.bottleDispenser.Bottle;
import com.example.viikko_8_app.bottleDispenser.BottleDispenser;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public TextView outputTxtV;
    TextView titleTxtV;
    TextView currentMoneyTxtV;

    Button insertMoneyBtn;
    Button returnMoneyBtn;
    Button buyBottleBtn;
    Button writeReceitBtn;

    Spinner spinner;

    SeekBar seekBar;

    BottleDispenser bd = new BottleDispenser();

    String selectedBottle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        outputTxtV = findViewById(R.id.outputText);
        titleTxtV = findViewById(R.id.bottleDispenserTitle);
        currentMoneyTxtV = findViewById(R.id.currentMoneyText);

        insertMoneyBtn = findViewById(R.id.insertMoneyButton);
        returnMoneyBtn = findViewById(R.id.returnMoneyButton);
        buyBottleBtn = findViewById(R.id.buyBottleButton);
        writeReceitBtn = findViewById(R.id.writeReceitButton);

        spinner = findViewById(R.id.spinner);

        seekBar = findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                outputTxtV.setText("About to add: "+progress+" €");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekBar.setMax(20);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.bottles, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedBottle = parent.getItemAtPosition(position).toString();
                //Toast.makeText(parent.getContext(), text, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @SuppressLint("SetTextI18n")
    public void addMoney(View v) {
        int moneyAdded = seekBar.getProgress();
        bd.addMoney(outputTxtV, currentMoneyTxtV, moneyAdded);
    }

    @SuppressLint("SetTextI18n")
    public void returnMoney(View v) {
        bd.returnMoney(outputTxtV, currentMoneyTxtV);
    }

    @SuppressLint("SetTextI18n")
    public void buyBottle(View v){
        selectedBottle =  selectedBottle.replace("€","");
        String[] bottleAttributes = selectedBottle.split(",");
        System.out.println("Main activity attributes: "+selectedBottle);
        bd.buyBottle(outputTxtV, currentMoneyTxtV, bottleAttributes);
    }



}