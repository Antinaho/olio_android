package com.example.viikko_8_app.bottleDispenser;

public class Bottle {

    private String name;
    private String manufacturer;
    private double total_energy;
    private double size;
    private double price;

    public Bottle(){
        this.name = "Pepsi Max";
        this.manufacturer = "Pepsi";
        this.total_energy = 0.3;
        this.size = 0.5;
        this.price = 1.8;
    }
    public Bottle(String name, String manuf, double totE, double size, double price){
        this.name = name;
        this.manufacturer = manuf;
        this.total_energy = totE;
        this.size = size;
        this.price = price;
    }

    public String getName(){
        return name;
    }

    public double getPrice() {
        return price;
    }

    public double getSize() {
        return size;
    }

}
